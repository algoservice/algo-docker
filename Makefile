# Variables
make_directory=make
make_help=$(make_directory)/Makefile-help
make_entrypoint=$(make_directory)/entrypoint.sh

.SILENT: help
help:
	less $(make_help)

.SILENT: up
up:
	bash $(make_entrypoint) up

.SILENT: up_fresh
up_fresh:
	bash $(make_entrypoint) up-fresh

.SILENT: down
down:
	bash $(make_entrypoint) down

.SILENT: restart
restart: down up

.SILENT: build
build:
	bash $(make_entrypoint) build

.SILENT: refresh
refresh: down build up_fresh

.SILENT: attach_web
attach_web:
	bash $(make_entrypoint) attach_web
