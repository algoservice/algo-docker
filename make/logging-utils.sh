#!/usr/bin/env bash

set -e +x

# General
## Names
script_name='[Algo-make]'
### Colors
script_log_name_color="\033[0;96m"
script_log_no_color="\033[0m"
script_log_info_color="\033[0;92m"
script_log_warning_color="\033[0;93m"
script_log_error_color="\033[0;91m"
### Variables
script_log_name="${script_log_name_color}${script_name}${script_log_no_color}"
script_log_datetime_format="+%Y-%m-%d %H:%M:%S"
script_log_level_info="${script_log_info_color}INFO${script_log_no_color}"
script_log_level_warning="${script_log_warning_color}WARN${script_log_no_color}"
script_log_level_error="${script_log_error_color}ERR${script_log_no_color}"
script_log_pattern="\${script_log_name} \$(date '${script_log_datetime_format}') \$1 : \$2"

# Functions
__log_general() {
  eval echo -e "$script_log_pattern"
}

log_i() {
  __log_general "$script_log_level_info" "$1"
}

log_w() {
  __log_general "$script_log_level_warning" "$1"
}

log_e() {
  __log_general "$script_log_level_error" "$1"
}
