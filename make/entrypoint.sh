#!/usr/bin/env bash

set -e +x

source "$(dirname "$0")/logging-utils.sh"
source "$(dirname "$0")/docker-utils.sh"
source "$(dirname "$0")/static.sh"

_up() {
  log_i "Starting..."
  docker compose up -d
  log_i "Done."
}

_up_fresh() {
  log_i "Starting fresh run..."
  docker compose up -d --force-recreate --no-deps
  log_i "Started successfully."
}

_down() {
  log_i "Stopping..."
  docker compose down
  log_i "Stopped successfully."
}

_build() {
  log_i "Rebuilding..."
  docker compose build --no-cache
  log_i "Rebuilt successfully."
}

_attach_web() {
  log_i "Checking if the $web_container is available for attach..."
  is_container_running "$web_container"
  log_i "Attaching to the $web_container..."
  docker attach "$web_container"
  log_i "Detached."
}

case $1 in
  up) _up;;
  up-fresh) _up_fresh;;
  down) _down;;
  build) _build;;
  attach_web) _attach_web;;
  *) exec "$@";;
esac