#!/usr/bin/env bash

set -e +x

source "$(dirname "$0")/logging-utils.sh"

is_container_exists() {
  log_i "Checking if the container $container_name exists..."
  local container_name="$1"
  if [ "$( docker ps -a | grep -c "$container_name" )" -gt 0 ]; then
    log_i "Container $container_name exists."
    return 0
  else
    log_e "Container $container_name does not exist!"
    return 1
  fi
}

is_container_running() {
  local container_name="$1"
  is_container_exists "$container_name"
  local is_exists=$?
  if [ $is_exists -eq 0 ]; then
    log_i "Checking if the container $container_name is running..."
    if [ "$(docker ps -q -f status=running -f name="$container_name")" ]; then
      log_i "Container $container_name is running."
      return 0
    else
      log_e "Container $container_name is not running!"
      return 1
    fi
  else
    return 1
  fi
}
